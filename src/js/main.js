var body = document.getElementsByTagName('body')[0];
var modal = document.getElementById('modal');

//intro animation
Animate = function(el){
    if (!(this instanceof Animate)) {
        return new Animate(el);
    }

    this._el = el;
    this._build();
}

Animate.prototype._build = function(){
    this._animate(this._getElements());
    var container = this._el;
    setTimeout(function(){
        container.classList.add('done');
    },4000);
}

Animate.prototype._animate = function(items){
    var index = 0;
    [].map.call(items, function(item){
        index ++;
        if (item.nodeType == 1) {
            item.style.transitionDelay = index+'s';
        }

    });
}

Animate.prototype._getElements = function(){
    var children = this._el.childNodes;
    return children;
}

var animators = document.querySelectorAll('[data-animate-content]');
for (var i = 0; i < animators.length; i++) {
    new Animate(animators[i]);
}



$("#detailslider").slick({
    arrows: false,
    dots: true
})

function initMap() {
    var mapDiv = document.getElementById('map');
    var map = new google.maps.Map(mapDiv, {
        center: {lat: 52.2139113, lng: 5.2817102},
        zoom: 14
    });

    var marker = new google.maps.Marker({
        position: map.getCenter(),
        icon: {
          path: google.maps.SymbolPath.CIRCLE,
          scale: 10
        },
        draggable: true,
        map: map
      });
}

var navButton = document.getElementById('btn--menu');
var mainNav = document.getElementById('nav');

navButton.addEventListener('click', function(){
    if(!mainNav.classList.contains('visible')){
        mainNav.classList.add('visible');
        body.classList.add('menu-active');
        navButton.classList.add('active');
    }else{
        mainNav.classList.remove('visible');
        navButton.classList.remove('active');
        body.classList.remove('menu-active');
    }
    
});



if (!!modal == true) {

    GalleryButton = function(el){
        if (!(this instanceof GalleryButton)) {
            return new GalleryButton(el);
        }
        var nextBtn = document.getElementById('next');
        var prevBtn = document.getElementById('prev');

        this._el = el;
        this._build(this._el);

        this._el.addEventListener('click', this._openModal, true);
        nextBtn.addEventListener('click', this._nextImage, true);
        prevBtn.addEventListener('click', this._prevImage, true);
    }

    GalleryButton.prototype._build = function(el){

        var images = this.getImages();
        this._displayImages(images);
        var currentImage = document.getElementById('modal').getAttribute('data-current-slide')
    }

    GalleryButton.prototype._openModal = function(){
        var modal = document.getElementById('modal');
        if (!modal.classList.contains('visible')) {
            modal.classList.add('visible');
        }
    }

    GalleryButton.prototype.getImages = function(){
        var container = document.getElementById('gallery');
        var images = container.getElementsByTagName('li');
            
        return images;
    }

    GalleryButton.prototype._displayImages = function(images){
        var parent = document.getElementById('modal');
        var container = parent.getElementsByTagName('div')[0];
        var image = document.createElement('img');
        image.src = '';

        [].map.call(images, function(img){
            if (img.getAttribute('data-index') === "1") {
                image.src = img.getAttribute('src');
                container.appendChild(image);
                parent.setAttribute('data-current-slide', img.getAttribute('data-index'));
            }
        });
    }

    GalleryButton.prototype._prevImage = function(images, currentIndex){
        var container = document.getElementById('gallery');
        var images = container.getElementsByTagName('li');
        var parent = document.getElementById('modal');

        if (parseInt(parent.getAttribute('data-current-slide')) > 1) {
            var contentContainer = parent.getElementsByTagName('div')[0];
            var nextSlideI = parseInt(parent.getAttribute('data-current-slide')) - 1;
            var nextSlide = container.querySelectorAll('[data-index="'+nextSlideI+'"]')[0];
            parent.setAttribute('data-current-slide', nextSlideI);
            contentContainer.getElementsByTagName('img')[0].setAttribute('src', nextSlide.getAttribute('src'))
        }    
    }

    GalleryButton.prototype._nextImage = function(){
        var container = document.getElementById('gallery');
        var images = container.getElementsByTagName('li');
        var parent = document.getElementById('modal');

        if (parseInt(parent.getAttribute('data-current-slide')) < images.length) {
            var contentContainer = parent.getElementsByTagName('div')[0];
            var nextSlideI = parseInt(parent.getAttribute('data-current-slide')) + 1;
            var nextSlide = container.querySelectorAll('[data-index="'+nextSlideI+'"]')[0];
            parent.setAttribute('data-current-slide', nextSlideI);
            contentContainer.getElementsByTagName('img')[0].setAttribute('src', nextSlide.getAttribute('src'));
        }
    }

    var galleryButtons = document.querySelectorAll('[data-open-gallery]');
    for (var i = galleryButtons.length - 1; i >= 0; i--) {
        new GalleryButton (galleryButtons[i]);
    }

    var modalClose = document.getElementById('close');
    modalClose.addEventListener('click', function(){
        modal.classList.remove('visible');
    });
}



var filterButton = document.getElementById('btn--filter');
var filterOptions = document.querySelectorAll('[data-filter-option]');
var selectedOptions = {
    surface: "0",
    type: "0",
    location: "0"
};

[].map.call(filterOptions, function(select){
    select.addEventListener('change', function(){
        var option = select.options[select.selectedIndex].value;
        if (select.getAttribute('data-type') == "surface") {
            selectedOptions['surface'] = option;
        }else if(select.getAttribute('data-type') == "type"){
            selectedOptions['type'] = option;
        }else if(select.getAttribute('data-type') == "location"){
            selectedOptions['location'] = option;
        }
    });
});

filterButton.addEventListener('click', function(){
   window.location.href = '?surface='+selectedOptions.surface+'&type='+selectedOptions.type+'&location='+selectedOptions.location+'';
});


var offers = document.querySelectorAll('[data-offer]');

var QueryString = function () {
  // This function is anonymous, is executed immediately and 
  // the return value is assigned to QueryString!
  var query_string = {};
  var query = window.location.search.substring(1);
  var vars = query.split("&");

  for (var i=0;i<vars.length;i++) {
    var pair = vars[i].split("=");
        // If first entry with this name
    if (typeof query_string[pair[0]] === "undefined") {
      query_string[pair[0]] = decodeURIComponent(pair[1]);
        // If second entry with this name
    } else if (typeof query_string[pair[0]] === "string") {
      var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
      query_string[pair[0]] = arr;
        // If third or later entry with this name
    } else {
      query_string[pair[0]].push(decodeURIComponent(pair[1]));
    }
  } 

  return query_string;
}();
var queries = QueryString;

var filterActive;
if (queries.surface != "0" && queries.type != "0" && queries.location != "0") {
    filterActive = false;
}else{
    filterActive = true;
}

if (filterActive === true) {
    [].map.call(offers, function(offer){
        if (offer.getAttribute('data-type') !== queries.type && queries.type != "0") {
            offer.remove();   
        }
        if (offer.getAttribute('data-surface') < queries.surface && queries.surface != "0") {
            offer.remove();   
        }
        if (offer.getAttribute('data-location') != queries.location && queries.location != "0") {
            offer.remove();   
        }
    });
}