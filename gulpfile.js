var gulp = require('gulp');
var sass = require('gulp-sass');
var twig = require('gulp-twig');
var concat = require('gulp-concat');
var include = require('gulp-include');
var prefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('default', ['styles', 'templates', 'assets', 'js', 'watch', 'serve'])

gulp.task('styles', function(){
    return gulp.src('src/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(prefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('app/assets/styles'));
})

gulp.task('js', function () {
    gulp.src('src/js/main.js')
        .pipe(include({
            includePaths: [
              "/src/js"
          ]
        }))
        .pipe(concat("app.js"))
        .pipe(gulp.dest('app/assets/js/'));
});

gulp.task('templates', function () {
    return gulp.src('src/html/*.html')
        .pipe(twig())
        .pipe(gulp.dest('app'))
});

gulp.task('assets', function () {
    gulp.src('src/images/**/*')
        .pipe(gulp.dest('app/assets/img/'));
});

gulp.task('serve', ['styles'], function() {
    
    browserSync.init({
        server: "./app"
    });

    gulp.watch("src/scss/*.scss", ['styles']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
});

gulp.task('watch', function() {
    // gulp.watch('src/js/vendor/*.js', ["js-vendor"]);
    gulp.watch('src/js/**/*.js', ["js"]);
    gulp.watch('src/scss/**/*.scss', ["styles"]);
    gulp.watch('src/html/**/*', ["templates"]);
    gulp.watch('src/images/**/*', ["assets"]);
});